#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return



HISTFILESIZE=400000000
HISTSIZE=10000
PROMPT_COMMAND="history -a"
shopt -s histappend




# Distrobox -- Coding in an Environment Container
export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:$HOME/.local/podman/bin
# TODO: Prevent it from doing whacky shit on every terminal open. 
xhost +SI:localuser:$USER

# Don't pollute the history with pointless merges
git config --global pull.rebase true
# Set an email and username
git config --global user.email kladivko.dev@gmail.com
git config --global user.name  Milan Kladivko

# nvm / npm / node
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
# bun
export BUN_INSTALL="$HOME/.bun"
export PATH=$BUN_INSTALL/bin:$PATH
# golang
export PATH=$PATH:$HOME/go/bin
