
Helix
=====

> My current full-time editor. Vim-ish. 
```sh
cd ~/.config/helix
ln -s ~/<whatever>/conf/helix/config.toml config.toml
```

No need to do any `runtime` folder linking,
as mentioned in the docs in build-from-source. 
The project gained many needed ways to install, so that is sorted. 


Emacs
=====
- [ ] Move the `.emacs.d` folder in here, it's in its own repo atm. 
- [ ] Pull out all the garbage -- I just need it for Magit !!
- [ ] Reconsider all the packages and `init.el` there, at least once. 

VSCode
======
> I stopped using VSCode so this is... Not gonna be updated. 
> I think the paths aren't even correct ...? Meh.

- `.config/User/settings.json` paddings, font, language preferences
- `.config/User/keybinds.json` custom keybinds
- `.vscode/extensions/` downloaded extensions

> The extensions are pre-downloaded so that I can use VSCode
> versions that do not have the shop -- VSCodium or such. 
> I was surprised once that one has to go through the actual VSCode
> to download the extensions but then it's fine to use the same
> extensions folder. 
> Now I'm ready to use whatever VSCode fork I want. 
> Also, this way nothing is forcing me to use the latest versions
> of the extensions -- I can just keep going with the old one.
> And I have a revert-state if an update breaks everything. 

## Usage

Symlink the files / folders and replace the original files with
the symlinks. 

I wouldn't advise, in general, having git contain the whole config
folder with some gitignores -- that didn't work out well. 

